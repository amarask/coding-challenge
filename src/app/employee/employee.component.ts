import {Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import {map} from 'rxjs/operators';
import {forkJoin} from 'rxjs';

import {Employee} from '../employee';
import {EmployeeService} from '../employee.service';

@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.css']
})

export class EmployeeComponent implements OnInit {
  // Referenced https://angular.io/guide/template-syntax#input-and-output-properties when adding the employee property
  @Input() employee: Employee;
  // Creating event emitter for deleting report. Referenced https://angular.io/api/core/EventEmitter
  @Output() deleteReport = new EventEmitter<Employee>();
  directReports: Employee[] = [];
  totalReports = 0;
  constructor(private employeeService: EmployeeService) {
  }

  // Function to recursively count reports for an employee
  countReports(e: Employee): void {
    const reports = e.directReports;
    if (reports) {
      this.totalReports = this.totalReports + reports.length;
      reports.forEach(id => (
        this.employeeService.get(id)
        .subscribe(report => this.countReports(report))
      ));
    }
  }

  // Function to initiate deleting the report
  delete(report:Employee): void {
    this.deleteReport.emit(report);
  }

  ngOnInit(): void {
    const reports = this.employee.directReports;
    if (reports) {
      forkJoin(reports.map(id => this.employeeService.get(id)))
      .subscribe(directReports => {
        this.directReports = directReports;
      });

      this.countReports(this.employee);
    }
  }
}
